package me.flyray.auth.common.bean;

import java.io.Serializable;

import lombok.Data;
import me.flyray.auth.common.util.jwt.IJWTInfo;


/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 登陆用户授权
**/

@Data
public class UserJWTInfo implements IJWTInfo,Serializable {
	
	/**
	 * 登陆用户名称
	 */
    private String name;
    /**
     * 用户id userId
     */
    private String XId;
    /**
     * 平台编号
     * */
    private String platformId;
    /**
     * 签名盐值
     * */
    private String saltKey;
    
    public UserJWTInfo(String platformId, String name,String nickname, String XId) {
    	this.platformId = platformId;
        this.name = name;
        this.XId = XId;
    }

    public UserJWTInfo(String platformId, String saltKey) {
		this.platformId = platformId;
		this.saltKey = saltKey;
	}

    @Override
    public String getXId() {
        return XId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserJWTInfo jwtInfo = (UserJWTInfo) o;

        if (name != null ? !name.equals(jwtInfo.name) : jwtInfo.name != null) {
            return false;
        }
        return XId != null ? XId.equals(jwtInfo.XId) : jwtInfo.XId == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (XId != null ? XId.hashCode() : 0);
        return result;
    }

	@Override
	public String getPlatformId() {
		return platformId;
	}

	@Override
	public String getSaltKey() {
		return saltKey;
	}

	@Override
	public String getClientCode() {
		return null;
	}

    @Override
    public String getNickname() {
        return null;
    }
}
