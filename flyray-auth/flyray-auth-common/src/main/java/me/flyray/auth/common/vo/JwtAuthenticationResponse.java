package me.flyray.auth.common.vo;


import lombok.Data;

import java.io.Serializable;

@Data
public class JwtAuthenticationResponse {

	public String platformId;

	public String userId;

	public String username;

	public String token;

	public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public JwtAuthenticationResponse() {
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}



}
