package me.flyray.auth.common.bean;


import lombok.Data;
import me.flyray.auth.common.util.jwt.IJWTInfo;

import java.io.Serializable;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 微服务客户端授权
**/

@Data
public class ClientJWTInfo implements IJWTInfo, Serializable {
	
	/**
	 * 微服务ID
	 */
	String XId;
	/**
	 * 微服务code
	 */
    String clientCode;
    /**
	 * 微服务名称
	 */
    String name;

    public ClientJWTInfo(String clientCode, String name, String XId) {
    	this.clientCode = clientCode;
        this.name = name;
        this.XId = XId;
    }

    @Override
    public String getXId() {
        return XId;
    }

	@Override
	public String getPlatformId() {
		return null;
	}

	@Override
	public String getSaltKey() {
		return null;
	}

	@Override
	public String getNickname() {
		return null;
	}

}
