package me.flyray.auth.common.util.jwt;

/**
 * Created by ace on 2017/9/10.
 */
public interface IJWTInfo {

    /**
     * 获取用户ID
     * @return
     */
    String getXId();

    /**
     * 获取微服务名称或登陆用户名称
     * @return
     */
    String getName();

    /**
     * 获取微服务名称或登陆用户昵称
     * @return
     */
    String getNickname();
    
    /**
     * 获取微服务code
     * @return
     */
    String getClientCode();
    
    /**
     * 获取平台编号
     * */
    String getPlatformId();
    /**
     * 获取签名盐值
     * */
    String getSaltKey();
}
