package me.flyray.auth.configuration;

import me.flyray.auth.interceptor.ServiceAuthRestInterceptor;
import me.flyray.auth.interceptor.UserAuthRestInterceptor;

import java.util.ArrayList;
import java.util.Collections;

import me.flyray.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author ace
 * @date 2017/9/8
 */
@Configuration("authWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {

    @Bean
    GlobalExceptionHandler getGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	registry.addInterceptor(getServiceAuthRestInterceptor()).addPathPatterns("/**").excludePathPatterns(getExcludeCommonPathPatterns().toArray(new String[]{}));
    	//registry.addInterceptor(getUserAuthRestInterceptor()).addPathPatterns("/**");
    	/*ArrayList<String> commonPathPatterns = getExcludeCommonPathPatterns();
        registry.addInterceptor(getServiceAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonPathPatterns.toArray(new String[]{}));
        registry.addInterceptor(getUserAuthRestInterceptor()).
        		addPathPatterns("/**").excludePathPatterns(commonPathPatterns.toArray(new String[]{}));*/
    }

    @Bean
    ServiceAuthRestInterceptor getServiceAuthRestInterceptor() {
        return new ServiceAuthRestInterceptor();
    }

    @Bean
    UserAuthRestInterceptor getUserAuthRestInterceptor() {
        return new UserAuthRestInterceptor();
    }
    
    private ArrayList<String> getExcludeCommonPathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/client/token",
                "/client/servicePubKey",
                "/client/userPubKey",
                "/v2/api-docs",
                "/swagger-resources/**",
                "/swagger-ui.html",
                "/webjars/**"
        };
        Collections.addAll(list, urls);
        return list;
    }

}
