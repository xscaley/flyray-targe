package me.flyray.auth.feignserver;

import me.flyray.auth.biz.ClientBiz;
import me.flyray.auth.entity.Client;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.rest.BaseController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ace
 * @create 2017/12/26.
 */
@RestController
@RequestMapping("service")
public class ServiceController extends BaseController<ClientBiz, Client>{

    @RequestMapping(value = "/{id}/client", method = RequestMethod.PUT)
    @ResponseBody
    public BaseApiResponse<Void> modifyUsers(@PathVariable int id, String clients){
        baseBiz.modifyClientServices(id, clients);
        return BaseApiResponse.newSuccess();
    }

    @RequestMapping(value = "/{id}/client", method = RequestMethod.GET)
    @ResponseBody
    public BaseApiResponse<List<Client>> getUsers(@PathVariable int id){
        return BaseApiResponse.newSuccess(baseBiz.getClientServices(id));
    }
}
