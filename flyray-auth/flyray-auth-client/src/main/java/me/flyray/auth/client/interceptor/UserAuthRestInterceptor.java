package me.flyray.auth.client.interceptor;

import me.flyray.auth.client.annotation.IgnoreUserToken;
import me.flyray.auth.client.jwt.UserAuthUtil;
import me.flyray.auth.common.config.UserAuthConfig;
import me.flyray.auth.common.util.jwt.IJWTInfo;
import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.exception.BusinessException;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.service.InputStreamReadRepeatableRequestWrapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by bolei on 2017/9/10.
 * 修改by bolei
 */
public class UserAuthRestInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(UserAuthRestInterceptor.class);

    @Value("${toplevel.platform.id}")
    private String topLevelPlatformId;

    @Autowired
    private UserAuthUtil userAuthUtil;

    @Autowired
    private UserAuthConfig userAuthConfig;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = null;
        if (handler instanceof HandlerMethod) {
            handlerMethod = (HandlerMethod) handler;
        } else {
            return super.preHandle(request, response, handler);
        }
        // 配置该注解，说明不进行用户拦截
        IgnoreUserToken annotation = handlerMethod.getBeanType().getAnnotation(IgnoreUserToken.class);
        if (annotation == null) {
            annotation = handlerMethod.getMethodAnnotation(IgnoreUserToken.class);
        }
        if (annotation != null) {
            return super.preHandle(request, response, handler);
        }
        String token = request.getHeader(userAuthConfig.getTokenHeader());
        if (StringUtils.isEmpty(token)) {
            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals(userAuthConfig.getTokenHeader())) {
                        token = cookie.getValue();
                    }
                }
            }
        }
        if (StringUtils.isEmpty(token)){
            throw new BusinessException(ResponseCode.TOKEN_ISNULL_CODE);
        }
        IJWTInfo infoFromToken = userAuthUtil.getInfoFromToken(token);
        //判断平台编号是否是顶级平台,如果 判断token与platformId是否一致
        if (topLevelPlatformId.equals(infoFromToken.getPlatformId())){
            request = new InputStreamReadRepeatableRequestWrapper((HttpServletRequest) request);
        }
        BaseContextHandler.setPlatformId(infoFromToken.getPlatformId());
        BaseContextHandler.setName(infoFromToken.getName());
        BaseContextHandler.setXId(infoFromToken.getXId());
        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        BaseContextHandler.remove();
        super.afterCompletion(request, response, handler, ex);
    }
}
