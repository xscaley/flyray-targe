package me.flyray.crm.facade.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 14:39 2019/1/11
 * @Description: 客户基础信息
 */

@Data
public class CustomerBaseResponse implements Serializable {

    private Integer id;

    //用户编号
    private String customerId;

    //平台编号
    private String platformId;

    //客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
    private String customerType;

    /**
     * 登陆用户名
     */
    private String username;

    //登陆密码
    private String password;

    //支付密码
    private String payPassword;

    //登录密码错误次数
    private Integer passwordErrorCount;

    //支付密码错误次数
    private Integer payPasswordErrorCount;

    //登录密码状态  00：正常   01：未设置   02：锁定
    private String passwordStatus;

    //支付密码状态    00：正常   01：未设置   02：锁定
    private String payPasswordStatus;

    //个人客户编号
    private String personalId;

    //商户客户编号
    private String merchantId;

    //账户状态 00：正常，01：客户冻结
    private String status;

    //认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
    private String authenticationStatus;

    //注册时间
    private Date registerTime;

    //注册IP
    private String registerIp;

    //上次登录时间
    private Date loginTime;

    //上次登录IP
    private String loginIp;

    //最后登录角色（默认为空）  01：个人客户     02 ：商户客户
    private String lastLoginRole;

    //创建时间
    private Date createTime;

    //更新时间
    private Date updateTime;

    //登录密码盐值
    private String passwordSalt;

    //支付密码盐值
    private String payPasswordSalt;
}
