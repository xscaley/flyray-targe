package me.flyray.crm.facade.request;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户注册请求参数
 */
public class PersonalGjjInfoReq implements Serializable {


	private String id;
	/**
	 * 客户会员号（个人客户编号）
	 */
	private String personalId;

	/**
	 * 平台编号
	 */
	@NotNull(message="合作平台编号不能为空")
	private String platformId;

	/**
	 * 职工姓名
	 */
	private String spname;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 身份证号码
	 */
	@NotNull(message="身份证号不能为空")
	private String spidNo;

	/**
	 * 账户状态
	 */
	private String zhzt;

	/**
	 * 手机号
	 */
	private String sjh;

	/**
	 * 所属机构名称
	 */
	private String orgName;

	/**
	 * 单位编码
	 */
	private String snCode;

	/**
	 * 单位名称
	 */
	private String snName;

	/**
	 * 职工账号
	 */
	private String spCode;

	/**
	 * 开户日期
	 */
	private String khrq;

	/**
	 * 工资基数
	 */
	private BigDecimal spgz;

	/**
	 * 个人缴存比例
	 */
	private BigDecimal spsingl;

	/**
	 * 单位缴存比例
	 */
	private BigDecimal spjcbl;

	/**
	 * 月汇缴额
	 */
	private BigDecimal spmfact;

	/**
	 * 余额
	 */
	private BigDecimal spmend;

	/**
	 * 汇缴年月
	 */
	private String spjym;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getSpname() {
		return spname;
	}

	public void setSpname(String spname) {
		this.spname = spname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSpidNo() {
		return spidNo;
	}

	public void setSpidNo(String spidNo) {
		this.spidNo = spidNo;
	}

	public String getZhzt() {
		return zhzt;
	}

	public void setZhzt(String zhzt) {
		this.zhzt = zhzt;
	}

	public String getSjh() {
		return sjh;
	}

	public void setSjh(String sjh) {
		this.sjh = sjh;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSnCode() {
		return snCode;
	}

	public void setSnCode(String snCode) {
		this.snCode = snCode;
	}

	public String getSnName() {
		return snName;
	}

	public void setSnName(String snName) {
		this.snName = snName;
	}

	public String getSpCode() {
		return spCode;
	}

	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	public String getKhrq() {
		return khrq;
	}

	public void setKhrq(String khrq) {
		this.khrq = khrq;
	}

	public BigDecimal getSpgz() {
		return spgz;
	}

	public void setSpgz(BigDecimal spgz) {
		this.spgz = spgz;
	}

	public BigDecimal getSpsingl() {
		return spsingl;
	}

	public void setSpsingl(BigDecimal spsingl) {
		this.spsingl = spsingl;
	}

	public BigDecimal getSpjcbl() {
		return spjcbl;
	}

	public void setSpjcbl(BigDecimal spjcbl) {
		this.spjcbl = spjcbl;
	}

	public BigDecimal getSpmfact() {
		return spmfact;
	}

	public void setSpmfact(BigDecimal spmfact) {
		this.spmfact = spmfact;
	}

	public BigDecimal getSpmend() {
		return spmend;
	}

	public void setSpmend(BigDecimal spmend) {
		this.spmend = spmend;
	}

	public String getSpjym() {
		return spjym;
	}

	public void setSpjym(String spjym) {
		this.spjym = spjym;
	}
}