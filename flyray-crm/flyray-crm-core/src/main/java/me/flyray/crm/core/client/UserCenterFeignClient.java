package me.flyray.crm.core.client;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "flyray-biz", configuration = {})
public interface UserCenterFeignClient {
	/**
	 * 提供给运营前端--猎手中心
	 */
	@RequestMapping(value = "feign/userCenter/hunterCenterInfoManager", method = RequestMethod.POST)
	public Map<String, Object> hunterCenterInfoManager(@RequestBody Map<String, Object> req);
	/** 
	 * 提供给运营前端--悬赏中心
	 */
	@RequestMapping(value = "feign/userCenter/rewardCenterInfoManager", method = RequestMethod.POST)
	public Map<String, Object> rewardCenterInfoManager(@RequestBody Map<String, Object> req);
}
