package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.MerchantBaseExt;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商户基础信息扩展表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantBaseExtMapper extends Mapper<MerchantBaseExt> {
	
}
