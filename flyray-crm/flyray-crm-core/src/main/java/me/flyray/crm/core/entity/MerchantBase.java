package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 商户客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "merchant_base")
@Data
public class MerchantBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Id
    private Integer id;
    
	    //商户编号
    @Column(name = "merchant_id")
    private String merchantId;
	

    //商户名称
	@Column(name = "merchant_name")
	private String merchantName;

	//商户类型
	@Column(name = "merchant_type")
	private String merchantType;

	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //第三方商户编号
    @Column(name = "third_no")
    private String thirdNo;
	
	    //父级商户编号
    @Column(name = "parent_mer_id")
    private String parentMerId;
	
	    //商户类型  00：普通商户  01：子商户 
    @Column(name = "merchant_level")
    private String merchantLevel;
	
	    //企业名称
    @Column(name = "company_name")
    private String companyName;
	
	    //经营范围
    @Column(name = "business_scope")
    private String businessScope;
	
	    //工商注册号
    @Column(name = "business_no")
    private String businessNo;
	
	    //法人姓名
    @Column(name = "legal_person_name")
    private String legalPersonName;
	
	    //法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
    @Column(name = "legal_person_cred_type")
    private String legalPersonCredType;
	
	    //法人证件号码
    @Column(name = "legal_person_cred_no")
    private String legalPersonCredNo;
	
	    //营业执照
    @Column(name = "business_licence")
    private String businessLicence;
	
	    //联系电话
    @Column(name = "phone")
    private String phone;
	
	    //企业座机
    @Column(name = "mobile")
    private String mobile;
	
	    //企业传真
    @Column(name = "fax")
    private String fax;
	
	    //企业网址
    @Column(name = "http_address")
    private String httpAddress;
	
	    //注册资金
    @Column(name = "registered_capital")
    private BigDecimal registeredCapital;
	
	    //企业地址
    @Column(name = "company_address")
    private String companyAddress;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    @Column(name = "owner")
    private String owner;

}
