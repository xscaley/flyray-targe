package me.flyray.crm.core.biz.customer;

import java.util.HashMap;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.util.SnowFlake;
import me.flyray.crm.core.client.FeignAdminClient;
import org.springframework.beans.factory.annotation.Autowired;

import me.flyray.auth.common.config.UserAuthConfig;
import me.flyray.auth.common.util.jwt.IJWTInfo;
import me.flyray.auth.common.util.jwt.JWTHelper;
import me.flyray.common.enums.UserType;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 平台基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CustomerBiz {
	
	@Autowired
	private FeignAdminClient feignRoleClient;
    @Autowired
    private UserAuthConfig userAuthConfig;

	/**
	 * 添加商户与平台公共方法
	 * 添加平台需传参数 ： platformName，platformId，type为2（int），token
	 * 添加商户需传参数 ： merchantName，merchantId，platformId，type为3（int），token
	 * @param param
	 * @throws Exception 
	 */
	public Map<String, Object> commonAdd(Map<String, Object> param) throws Exception {
		String platformName = (String) param.get("platformName");
		String platformLoginName = (String) param.get("platformLoginName");
		String merchantName = (String) param.get("merchantName");
		String platformId = (String) param.get("platformId");
		String merchantId = (String) param.get("merchantId");
		Integer type = (Integer) param.get("type");
		String token = (String) param.get("token");
		String userNo = null;
		String userName = null;
		IJWTInfo info = JWTHelper.getInfoFromToken(token, userAuthConfig.getPubKeyByte());
		userNo = info.getXId();
		userName = info.getName();
		if(userNo == null || userName == null){
			throw new Exception();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		//dept表添加
		Map<String, Object> deptMap = new HashMap<String, Object>();
		deptMap.put("platformId", Long.valueOf(platformId));
		//如果是平台，平台的父级固定是 海盗集团，id为1，如果修改了最高级目录则要修改修改此处值
		Integer parentId = 1;
		//根据平台编号获取父机构编号
		if(UserType.PLATFORM_ADMIN.getCode() != type){
			Map<String, Object> deptPMap = new HashMap<String, Object>();
			deptPMap.put("parentId", 1);
			deptPMap.put("platformId",Long.valueOf(platformId));
			Map<String, Object> deptPResultMap = feignRoleClient.selectByPlatformId(deptPMap);
			String deptPResultCode = (String) deptPResultMap.get("code");
			if(!BizResponseCode.OK.getCode().equals(deptPResultCode)){
				throw new Exception();
			}
			Map<String, Object> mapDept = (Map<String, Object>) deptPResultMap.get("dept");
			Integer deptId = (Integer) mapDept.get("id");
			parentId = deptId;
			
		}
		deptMap.put("parentId", parentId);
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			deptMap.put("name", platformName);
		}else {
			deptMap.put("name", merchantName);
		}
		
		deptMap.put("delFlag", "0");
		Map<String, Object> deptResultMap = feignRoleClient.addDept(deptMap);
		String deptCode = (String) deptResultMap.get("code");
		if(!BizResponseCode.OK.getCode().equals(deptCode)){
	    	throw new Exception();
		}
		Integer deptId = (Integer) deptResultMap.get("deptId");
		//添加角色
		Map<String, Object> roleMap = new HashMap<String, Object>();
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			roleMap.put("roleName", platformName + "平台管理员");
		}else {
			roleMap.put("roleName", merchantName + "商户管理员");
		}
		
		roleMap.put("remark", "创建平台自动生成角色");
		roleMap.put("deptId", deptId);
		roleMap.put("platformId", platformId);
		roleMap.put("isDelete", 1);
		Map<String, Object> roleResultMap = feignRoleClient.addRole(roleMap);
		String roleCode = (String) roleResultMap.get("code");
		if(!BizResponseCode.OK.getCode().equals(roleCode)){
			throw new Exception();
		}
		Integer roleId = (Integer) roleResultMap.get("roleId");
		//添加用户
		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("userId", SnowFlake.getId());
		userMap.put("crtUser", userNo);
		userMap.put("crtName", userName);
		userMap.put("updUser", userNo);
		userMap.put("updName", userName);
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			userMap.put("username", platformLoginName);
		}else {
			userMap.put("username", merchantName);
		}
		//默认密码123456
		userMap.put("password", "123456");
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			userMap.put("name", platformName);
		}else {
			userMap.put("name", merchantName);
		}
		userMap.put("deptId", deptId);
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			userMap.put("deptName", platformName);
		}else {
			userMap.put("deptName", merchantName);
		}
		userMap.put("description", "添加平台自动添加的平台管理员");
		//userMap.put("crtTime", new Date());
		if(UserType.PLATFORM_ADMIN.getCode() != type){
			userMap.put("merchantId", merchantId);
		}
		userMap.put("platformId", platformId);
		if(UserType.PLATFORM_ADMIN.getCode() == type){
			userMap.put("userType", UserType.PLATFORM_ADMIN.getCode());
		}else {
			userMap.put("userType", UserType.MERCHANT_ADMIN.getCode());
		}
		
		Map<String, Object> userResultMap = feignRoleClient.addUser(userMap);
		String userCode = (String) userResultMap.get("code");
		if(!BizResponseCode.OK.getCode().equals(userCode)){
			throw new Exception();
		}
		Long userId = (Long) userResultMap.get("userId");
		//添加关系
		Map<String, Object> userRoleMap = new HashMap<String, Object>();
		userRoleMap.put("userId", userId);
		userRoleMap.put("roleId", roleId);
		Map<String, Object> userRoleResultMap = feignRoleClient.addUserRole(userRoleMap);
		String userRoleCode = (String) userRoleResultMap.get("code");
		if(!BizResponseCode.OK.getCode().equals(userRoleCode)){
			throw new Exception();
		}
		//资源
		Map<String, Object> resourceAuthorityMap = new HashMap<String, Object>();
		resourceAuthorityMap.put("roleId", roleId);
		Map<String, Object> resourceAuthorityResultMap = feignRoleClient.platformAuthority(resourceAuthorityMap);
		String resourceAuthorityCode = (String) resourceAuthorityResultMap.get("code");
		if(!BizResponseCode.OK.getCode().equals(resourceAuthorityCode)){
			throw new Exception();
		}
		result.put("code", BizResponseCode.OK.getCode());
    	result.put("message", BizResponseCode.OK.getMessage());
    	result.put("success", true);
    	return result;
	}
}