package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 用户消息关联表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:50
 */
@Table(name = "customer_message_rel")
public class CustomerMessageRel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private String id;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //消息编号
    @Column(name = "message_id")
    private String messageId;
	
	    //是否已读  00：未读  01：已读
    @Column(name = "isRead")
    private String isread;
	
	    //是否删除  00：正常  01：删除
    @Column(name = "isDelete")
    private String isdelete;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：消息编号
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	/**
	 * 获取：消息编号
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * 设置：是否已读  00：未读  01：已读
	 */
	public void setIsread(String isread) {
		this.isread = isread;
	}
	/**
	 * 获取：是否已读  00：未读  01：已读
	 */
	public String getIsread() {
		return isread;
	}
	/**
	 * 设置：是否删除  00：正常  01：删除
	 */
	public void setIsdelete(String isdelete) {
		this.isdelete = isdelete;
	}
	/**
	 * 获取：是否删除  00：正常  01：删除
	 */
	public String getIsdelete() {
		return isdelete;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
