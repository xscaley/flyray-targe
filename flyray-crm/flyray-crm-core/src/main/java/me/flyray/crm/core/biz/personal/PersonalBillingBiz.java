package me.flyray.crm.core.biz.personal;

import me.flyray.crm.core.entity.PersonalBilling;
import me.flyray.crm.core.mapper.PersonalBillingMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 个人账单
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalBillingBiz extends BaseBiz<PersonalBillingMapper, PersonalBilling> {
}