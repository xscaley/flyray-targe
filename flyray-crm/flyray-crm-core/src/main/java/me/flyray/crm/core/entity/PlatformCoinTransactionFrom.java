package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_coin_transaction_from")
public class PlatformCoinTransactionFrom implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //平台编号
    @Id
    private Long platformId;
	
	    //出账地址
    @Column(name = "from_address")
    private String fromAddress;
	
	    //交易金额
    @Column(name = "amount")
    private BigDecimal amount;
	
	    //1：奖励 2：转账 3：手续费
    @Column(name = "transaction_type")
    private String transactionType;
	
	    //各自平台的交易排序编号
    @Column(name = "serial_number")
    private Long serialNumber;
	
	    //交易hash
    @Column(name = "hash")
    private String hash;
	
	    //交易所属区块hash
    @Column(name = "block_hash")
    private String blockHash;
	
	    //交易签名
    @Column(name = "sign")
    private String sign;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	

	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：出账地址
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	/**
	 * 获取：出账地址
	 */
	public String getFromAddress() {
		return fromAddress;
	}
	/**
	 * 设置：交易金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：1：奖励 2：转账 3：手续费
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * 获取：1：奖励 2：转账 3：手续费
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * 设置：各自平台的交易排序编号
	 */
	public void setSerialNumber(Long serialNumber) {
		this.serialNumber = serialNumber;
	}
	/**
	 * 获取：各自平台的交易排序编号
	 */
	public Long getSerialNumber() {
		return serialNumber;
	}
	/**
	 * 设置：交易hash
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}
	/**
	 * 获取：交易hash
	 */
	public String getHash() {
		return hash;
	}
	/**
	 * 设置：交易所属区块hash
	 */
	public void setBlockHash(String blockHash) {
		this.blockHash = blockHash;
	}
	/**
	 * 获取：交易所属区块hash
	 */
	public String getBlockHash() {
		return blockHash;
	}
	/**
	 * 设置：交易签名
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}
	/**
	 * 获取：交易签名
	 */
	public String getSign() {
		return sign;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
}
