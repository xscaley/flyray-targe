package me.flyray.crm.core.biz.merchant;

import me.flyray.crm.core.entity.MerchantBaseExt;
import me.flyray.crm.core.mapper.MerchantBaseExtMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 商户基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class MerchantBaseExtBiz extends BaseBiz<MerchantBaseExtMapper, MerchantBaseExt> {
}