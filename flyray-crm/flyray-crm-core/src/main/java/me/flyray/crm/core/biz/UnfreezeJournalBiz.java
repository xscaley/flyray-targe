package me.flyray.crm.core.biz;

import java.util.List;

import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.core.mapper.UnfreezeJournalMapper;
import me.flyray.crm.facade.request.customerAccountJournalRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 解冻流水表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class UnfreezeJournalBiz extends BaseBiz<UnfreezeJournalMapper, UnfreezeJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(UnfreezeJournalBiz.class);
	
	public TableResultResponse<UnfreezeJournal> queryUnfreezeJournals(customerAccountJournalRequest param) {
		logger.info("查询解冻流水表,请求参数。。。{}"+param);
		Example example = new Example(UnfreezeJournal.class);
		Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId", param.getPlatformId());
		}
		if (!StringUtils.isEmpty(param.getTradeType())) {
			criteria.andEqualTo("tradeType", param.getTradeType());
		}
		if (!StringUtils.isEmpty(param.getOrderNo())) {
			criteria.andEqualTo("orderNo", param.getOrderNo());
		}
		example.setOrderByClause("create_time DESC");
		Page<UnfreezeJournal> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<UnfreezeJournal> list = mapper.selectByExample(example);
		TableResultResponse<UnfreezeJournal> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
}