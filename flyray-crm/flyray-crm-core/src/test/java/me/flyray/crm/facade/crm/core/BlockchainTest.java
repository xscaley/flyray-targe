package me.flyray.crm.facade.crm.core;

import me.flyray.crm.core.blockchain.ERC827Contract;
import me.flyray.crm.core.blockchain.transaction.Transaction;
import me.flyray.crm.core.blockchain.util.BtcAddressUtils;
import me.flyray.crm.core.blockchain.util.KeyUtil;
import me.flyray.crm.core.blockchain.wallet.Wallet;
import me.flyray.crm.core.blockchain.wallet.WalletUtils;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;

/**
 * 测试
 *
 * @author bolei
 * @date 2018/09/08
 */
public class BlockchainTest {

    public static void main(String[] args) {
        try {
        	//创建钱包
        	System.out.println(ERC827Contract.createWallet().getAddress());
        	
        	Wallet wallet = ERC827Contract.createWallet();
        	
        	String address = wallet.getAddress();
        	BCECPrivateKey privateKey = wallet.getPrivateKey();
        	//用户私钥保存私钥用户验证交易
        	String privateKeyStr = KeyUtil.privateKeyToString(privateKey);
        	
        	System.out.println(ERC827Contract.createWallet().getAddress());
        	
        	String address1 = ERC827Contract.createWallet().getAddress();
        	 
        	System.out.println(ERC827Contract.createWallet().getPrivateKey());
        	//给钱包入账 打包奖励需要收取手续费
        	ERC827Contract.transfer(address, "10");
        	
        	//查出地址余额
        	System.out.println(ERC827Contract.balanceOf(address));
        	
        	//手续费问题
        	
        	
        	//根据地址获取公钥
        	Wallet senderWallet = WalletUtils.getInstance().getWallet(address);
            byte[] pubKey = senderWallet.getPublicKey();
            byte[] pubKeyHash = BtcAddressUtils.ripeMD160Hash(pubKey);
        	
        	//地址之间转账
        	ERC827Contract.transferFrom(address, address1, "5");
        	
        	
        	//打包交易生成区块
        	//210个交易写一个块 每块交易奖励 21火源
        	//根据原力和火钻的排行前2100名地址 分配奖励
        	Transaction[] transactions = null;
        	ERC827Contract.createBlockchain(transactions);
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
