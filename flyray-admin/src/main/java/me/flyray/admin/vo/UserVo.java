package me.flyray.admin.vo;

import lombok.Data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

/** 
* @author: bolei
* @date：2018年4月17日 上午11:47:54 
* @description：类说明
*/

@Data
public class UserVo {

    private Integer id;

    private String username;

    private String password;

    private String name;
    
    private String areaCode;
    
	/**
     * 所属部门机构ID
     */
    private Integer deptId;

    /**
     * 所属部门机构名称
     */
    private String deptName;

    private String birthday;

    private String address;

    private String mobilePhone;

    private String telPhone;

    private String email;

    private String sex;

    private String type;

    private String status;

    private String description;

    private Date crtTime;

    private String crtUser;

    private String crtName;

    private String crtHost;

    private Date updTime;

    private String updUser;

    private String updName;

    private String updHost;
    
    private String roleId;

    /**
     * 用户权限1，系统管理员2，平台管理员3，商户管理员4,平台操作员
     */
    private Integer userType;

}
