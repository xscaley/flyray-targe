package me.flyray.admin.config;

import me.flyray.admin.interceptor.UserTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ace on 2017/9/12.
 */
@Configuration
public class FeignConfiguration {
    @Bean
    UserTokenInterceptor getClientTokenInterceptor(){
        return new UserTokenInterceptor();
    }
}
