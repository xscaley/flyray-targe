package me.flyray.admin.rpc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import me.flyray.admin.vo.BaseUserSkinStyleRequest;
import me.flyray.admin.constant.AdminCommonConstant;
import me.flyray.admin.entity.BaseUserSkinStyle;
import me.flyray.admin.entity.Element;
import me.flyray.admin.entity.Menu;
import me.flyray.admin.entity.User;
import me.flyray.admin.vo.FrontUser;
import me.flyray.admin.vo.MenuTree;
import me.flyray.auth.common.user.UserInfo;
import me.flyray.common.constant.CommonConstants;
import me.flyray.common.vo.PermissionInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import me.flyray.admin.biz.BaseUserSkinStyleBiz;
import me.flyray.admin.biz.ElementBiz;
import me.flyray.admin.biz.MenuBiz;
import me.flyray.admin.biz.UserBiz;
import me.flyray.admin.biz.UserRoleBiz;
import me.flyray.auth.client.jwt.UserAuthUtil;
import me.flyray.common.constant.UserConstant;
import me.flyray.common.util.TreeUtil;
import org.springframework.util.DigestUtils;

/**
 * Created by ace on 2017/9/12.
 */
@Service
public class PermissionService {
    @Autowired
    private UserBiz userBiz;
    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private UserRoleBiz userRoleBiz;
    @Autowired
    private ElementBiz elementBiz;
    @Autowired
	private BaseUserSkinStyleBiz baseUserSkinStyleBiz;
    @Autowired
    private UserAuthUtil userAuthUtil;
    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(UserConstant.PW_ENCORDER_SALT);


    public UserInfo getUserByUsername(String username) {
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByUsername(username);
        BeanUtils.copyProperties(user, info);
        info.setId(user.getId().toString());
        return info;
    }

    public UserInfo getUserByMobilePhone(String mobilePhone) {
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByMobilePhone(mobilePhone);
        BeanUtils.copyProperties(user, info);
        info.setId(user.getId().toString());
        return info;
    }
    
    public UserInfo validate(String username, String password){
        UserInfo info = new UserInfo();
        User user = userBiz.getUserByUsername(username);
        String md5Password = DigestUtils.md5DigestAsHex((DigestUtils.md5DigestAsHex(password.getBytes())+UserConstant.PW_MD5_SALT).getBytes());
        if(user != null){
    	 if (encoder.matches(md5Password, user.getPassword())) {
             BeanUtils.copyProperties(user, info);
             info.setUserId(String.valueOf(user.getUserId()));
             info.setNickname(user.getDeptName());
             BaseUserSkinStyleRequest baseUserSkinStyleRequest = new BaseUserSkinStyleRequest();
     		 baseUserSkinStyleRequest.setPlatformId(user.getPlatformId());
     		 baseUserSkinStyleRequest.setUserId(user.getUserId());
     		 Map<String, Object> respMap = baseUserSkinStyleBiz.queryObj(baseUserSkinStyleRequest);
     		 BaseUserSkinStyle baseUserSkinStyle = (BaseUserSkinStyle) respMap.get("baseUserSkinStyle");
     		 if (null != baseUserSkinStyle && null != baseUserSkinStyle.getSkinStyle()) {
				info.setSkinStyle(baseUserSkinStyle.getSkinStyle());
			} else {
				info.setSkinStyle("");
			}
         }
        }
        return info;
    }

    public List<PermissionInfo> getAllPermission() {
        List<Menu> menus = menuBiz.selectListAll();
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.getAllElementPermissions();
        element2permission(result, elements);
        return result;
    }

    private void menu2permission(List<Menu> menus, List<PermissionInfo> result) {
        PermissionInfo info;
        for (Menu menu : menus) {
            if (StringUtils.isBlank(menu.getHref())) {
                menu.setHref("/" + menu.getCode());
            }
            info = new PermissionInfo();
            info.setCode(menu.getCode());
            info.setType(AdminCommonConstant.RESOURCE_TYPE_MENU);
            info.setName(AdminCommonConstant.RESOURCE_ACTION_VISIT);
            String uri = menu.getHref();
            if (!uri.startsWith("/")) {
                uri = "/" + uri;
            }
            info.setTitle(menu.getTitle());
            info.setUri(uri);
            info.setMethod(AdminCommonConstant.RESOURCE_REQUEST_METHOD_GET);
            result.add(info);
            info.setMenu(menu.getTitle());
        }
    }

    
    /**
     * 
     * @param username
     * @return
     */
    public List<PermissionInfo> getPermissionByRole(String username) {
        User user = userBiz.getUserByUsername(username);
        //TODO 目前一个用户一个角色通过用户获取角色ID 具体逻辑通过sql实现
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getUserId());
        List<PermissionInfo> result = new ArrayList<PermissionInfo>();
        PermissionInfo info = null;
        menu2permission(menus, result);
        List<Element> elements = elementBiz.getAuthorityElementByUserId(user.getUserId() + "");
        element2permission(result, elements);
        return result;
    }

    private void element2permission(List<PermissionInfo> result, List<Element> elements) {
        PermissionInfo info;
        for (Element element : elements) {
            info = new PermissionInfo();
            info.setCode(element.getCode());
            info.setType(element.getType());
            info.setUri(element.getUri());
            info.setMethod(element.getMethod());
            info.setName(element.getName());
            info.setMenu(element.getMenuId());
            result.add(info);
        }
    }


    private List<MenuTree> getMenuTree(List<Menu> menus, int root) {
        List<MenuTree> trees = new ArrayList<MenuTree>();
        MenuTree node = null;
        for (Menu menu : menus) {
            node = new MenuTree();
            BeanUtils.copyProperties(menu, node);
            trees.add(node);
        }
        return TreeUtil.bulid(trees, root);
    }

    public FrontUser getUserInfo(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getName();
        if (username == null) {
            return null;
        }
        UserInfo user = this.getUserByUsername(username);
        FrontUser frontUser = new FrontUser();
        BeanUtils.copyProperties(user, frontUser);
        List<PermissionInfo> permissionInfos = this.getPermissionByRole(username);
        Stream<PermissionInfo> menus = permissionInfos.parallelStream().filter((permission) -> {
            return permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setMenus(menus.collect(Collectors.toList()));
        Stream<PermissionInfo> elements = permissionInfos.parallelStream().filter((permission) -> {
            return !permission.getType().equals(CommonConstants.RESOURCE_TYPE_MENU);
        });
        frontUser.setElements(elements.collect(Collectors.toList()));
        return frontUser;
    }

    public List<MenuTree> getMenusByUsername(String token) throws Exception {
        String username = userAuthUtil.getInfoFromToken(token).getName();
        if (username == null) {
            return null;
        }
        User user = userBiz.getUserByUsername(username);
        List<Menu> menus = menuBiz.getUserAuthorityMenuByUserId(user.getUserId());
        return getMenuTree(menus,AdminCommonConstant.ROOT);
    }
}
