package me.flyray.admin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "base_platform")
public class Platform {
    @Id
    private Integer id;

    /**
     * 平台id
     */
    @Column(name = "platform_id")
    private String platformId;

    /**
     * 平台key
     */
    @Column(name = "platform_key")
    private String platformKey;

    /**
     * 平台名称
     */
    @Column(name = "platform_name")
    private String platformName;

    /**
     * 平台简介
     */
    @Column(name = "platform_intro")
    private String platformIntro;

    /**
     * 操作时间
     */
    @Column(name = "crt_time")
    private Date crtTime;

    /**
     * 更新时间
     */
    @Column(name = "upt_time")
    private Date uptTime;

    /**
     * 操作人
     */
    @Column(name = "crt_name")
    private String crtName;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取平台id
     *
     * @return platform_id - 平台id
     */
    public String getPlatformId() {
        return platformId;
    }

    /**
     * 设置平台id
     *
     * @param platformId 平台id
     */
    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    /**
     * 获取平台key
     *
     * @return platform_key - 平台key
     */
    public String getPlatformKey() {
        return platformKey;
    }

    /**
     * 设置平台key
     *
     * @param platformKey 平台key
     */
    public void setPlatformKey(String platformKey) {
        this.platformKey = platformKey;
    }

    /**
     * 获取平台名称
     *
     * @return platform_name - 平台名称
     */
    public String getPlatformName() {
        return platformName;
    }

    /**
     * 设置平台名称
     *
     * @param platformName 平台名称
     */
    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    /**
     * 获取平台简介
     *
     * @return platform_intro - 平台简介
     */
    public String getPlatformIntro() {
        return platformIntro;
    }

    /**
     * 设置平台简介
     *
     * @param platformIntro 平台简介
     */
    public void setPlatformIntro(String platformIntro) {
        this.platformIntro = platformIntro;
    }

    /**
     * 获取操作时间
     *
     * @return crt_time - 操作时间
     */
    public Date getCrtTime() {
        return crtTime;
    }

    /**
     * 设置操作时间
     *
     * @param crtTime 操作时间
     */
    public void setCrtTime(Date crtTime) {
        this.crtTime = crtTime;
    }

    /**
     * 获取更新时间
     *
     * @return upt_time - 更新时间
     */
    public Date getUptTime() {
        return uptTime;
    }

    /**
     * 设置更新时间
     *
     * @param uptTime 更新时间
     */
    public void setUptTime(Date uptTime) {
        this.uptTime = uptTime;
    }

    /**
     * 获取操作人
     *
     * @return crt_name - 操作人
     */
    public String getCrtName() {
        return crtName;
    }

    /**
     * 设置操作人
     *
     * @param crtName 操作人
     */
    public void setCrtName(String crtName) {
        this.crtName = crtName;
    }
}