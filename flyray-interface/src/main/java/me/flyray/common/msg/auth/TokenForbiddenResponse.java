package me.flyray.common.msg.auth;

import me.flyray.common.constant.CommonConstants;
import me.flyray.common.msg.BaseApiResponse;

/**
 * Created by ace on 2017/8/25.
 */
public class TokenForbiddenResponse  extends BaseApiResponse {

    public TokenForbiddenResponse(String message) {
        super(String.valueOf(CommonConstants.EX_USER_INVALID_CODE), message);
    }
}
