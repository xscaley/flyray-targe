package me.flyray.common.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: bolei
 * @date: 19:21 2018/12/1
 * @Description: 接口公共返回参数标准
 */

@Data
public class BaseApiResponse<T> implements Serializable {

    private static final long serialVersionUID = -4505655308965878999L;

    //请求成功返回码为：0000
    private static final String successCode = "0000";
    //返回数据
    private T data;
    //返回码
    private String code;
    //返回描述
    private String message;

    //返回成功标识
    private Boolean success;

    /**
     * 推荐使用封装的静态方法newXXX方法进行构造对象
     */
    @Deprecated
    public BaseApiResponse(){
        this.success = true;
        this.code = successCode;
        this.message = "请求成功";
    }

    /**
     * 推荐使用封装的静态方法newXXX方法进行构造对象
     */
    @Deprecated
    public BaseApiResponse(String code, String message){
        this();
        this.success = false;
        this.code = code;
        this.message = message;
    }

    private BaseApiResponse(Boolean success,String code, String message){
        this.success = success;
        this.code = code;
        this.message = message;
    }

    private BaseApiResponse(String code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 推荐使用封装的静态方法newXXX方法进行构造对象
     */
    @Deprecated
    public BaseApiResponse(T data){
        this();
        this.data = data;
    }

    /**
     * 根据基本返回码来构造返回对象,仅供暴露外部的静态方法调用
     */
    private <E extends ReturnCode> BaseApiResponse(E responseCode) {
        this.success = (responseCode == ResponseCode.OK);
        this.code = responseCode.getReturnCode();
        this.message = responseCode.getReturnMessage();
    }
    /**
     * 创建一个成功返回（不带返回数据）
     */
    public static <T> BaseApiResponse<T> newSuccess() {
        return new BaseApiResponse<>(ResponseCode.OK);
    }
    /**
     * 创建一个成功返回（可以指定返回数据）
     */
    public static <T> BaseApiResponse<T> newSuccess(T data) {
        BaseApiResponse<T> baseApiResponse = newSuccess();
        baseApiResponse.setData(data);
        return baseApiResponse;
    }
    /**
     * 创建一个失败返回（不带返回数据）
     */
    public static <E extends ReturnCode, T> BaseApiResponse<T> newFailure(E responseCode) {
        return new BaseApiResponse<>(responseCode);
    }

    /**
     * 创建一个失败返回（带返回数据）
     */
    public static <E extends ReturnCode, T> BaseApiResponse<T> newFailure(String code, String message) {
        return new BaseApiResponse<>(false, code, message);
    }

    /**
     * 创建一个失败返回（带返回数据）
     */
    public static <E extends ReturnCode, T> BaseApiResponse<T> newFailure(E responseCode, T data) {
        BaseApiResponse<T> baseApiResponse = newFailure(responseCode);
        baseApiResponse.setData(data);
        return baseApiResponse;
    }

    /**
     * 从其他api返回码中拷贝返回码code与message
     */
    public static <T> BaseApiResponse<T> copyFrom(BaseApiResponse<?> other) {
        return new BaseApiResponse<>(other.getSuccess(), other.getCode(), other.getMessage());
    }

    /**
     * 从其他api返回码中拷贝返回码code与message，并附加data数据
     */
    public static <T> BaseApiResponse<T> copyFrom(BaseApiResponse<?> other, T data) {
        BaseApiResponse<T> baseApiResponse = copyFrom(other);
        baseApiResponse.setData(data);
        return baseApiResponse;
    }

}
